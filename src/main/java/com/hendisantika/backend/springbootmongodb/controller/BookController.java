package com.hendisantika.backend.springbootmongodb.controller;

import com.hendisantika.backend.springbootmongodb.model.Book;
import com.hendisantika.backend.springbootmongodb.repository.BookMongoRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-19
 * Time: 07:51
 */
@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
@Log4j2
public class BookController {

    @Autowired
    BookMongoRepository bookRepository;

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        log.info("Get all Books...");

        return bookRepository.findAll();
    }

    @PostMapping("/books/create")
    public Book createBook(@Valid @RequestBody Book book) {
        log.info("Create Book: " + book.getTitle() + "...");

        return bookRepository.save(book);
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<Book> getBook(@PathVariable("id") String id) {
        log.info("Get Book by id...");

        Optional<Book> bookData = bookRepository.findById(id);
        if (bookData.isPresent()) {
            return new ResponseEntity<>(bookData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<Book> updateBook(@PathVariable("id") String id, @RequestBody Book book) {
        log.info("Update Book with ID = " + id + "...");

        Optional<Book> bookData = bookRepository.findById(id);
        if (bookData.isPresent()) {
            Book savedBook = bookData.get();
            savedBook.setTitle(book.getTitle());
            savedBook.setAuthor(book.getAuthor());
            savedBook.setDescription(book.getDescription());
            savedBook.setPublished(book.getPublished());

            Book updatedBook = bookRepository.save(savedBook);
            return new ResponseEntity<>(updatedBook, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable("id") String id) {
        log.info("Delete Book with ID = " + id + "...");

        try {
            bookRepository.deleteById(id);
        } catch (Exception e) {
            return new ResponseEntity<>("Fail to delete!", HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity<>("Book has been deleted!", HttpStatus.OK);
    }

    @DeleteMapping("/books/delete")
    public ResponseEntity<String> deleteAllBooks() {
        log.info("Delete All Books...");

        try {
            bookRepository.deleteAll();
        } catch (Exception e) {
            return new ResponseEntity<>("Fail to delete!", HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity<>("All Books have been deleted!", HttpStatus.OK);
    }
}
