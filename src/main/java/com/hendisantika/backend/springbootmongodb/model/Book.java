package com.hendisantika.backend.springbootmongodb.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-19
 * Time: 07:50
 */

@Data
@ToString
@Document(collection = "books")
public class Book {
    @Id
    private String id;
    private String title;
    private String description;
    private String author;
    private int published;
}
