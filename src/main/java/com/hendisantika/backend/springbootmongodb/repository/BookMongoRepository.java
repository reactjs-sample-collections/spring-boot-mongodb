package com.hendisantika.backend.springbootmongodb.repository;

import com.hendisantika.backend.springbootmongodb.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-19
 * Time: 07:51
 */
public interface BookMongoRepository extends MongoRepository<Book, String> {
}

